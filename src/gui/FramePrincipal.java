package gui;

import java.sql.Connection;
import controller.FilmeController;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Filme;
import tool.FabricaBanco;

public class FramePrincipal extends javax.swing.JFrame {
    
    private RegrasTabelaFilme minhasRegras;
    private FilmeController controller;

    public FramePrincipal() {
        
        initComponents();
        
        meuInitComponents();
        
        controller = new FilmeController();
        
        /*Connection conexaoBanco = FabricaBanco.getConexaoPostgres();
        
        try {
            if(!conexaoBanco.isClosed()){
                
                System.out.println("Sucesso!!! :D");
                
            }else{
                
                System.out.println("Erro :(");
                
            }
        } catch (SQLException ex) {
            
            Logger.getLogger(FramePrincipal.class.getName()).log(Level.SEVERE, null, ex);
            
        }*/
        
    }

    private void meuInitComponents(){
        
        this.minhasRegras = new RegrasTabelaFilme();
        
        //minha tabela segue as regras implementadas na classe
        filmesTable.setModel(minhasRegras);
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        filmesTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        nomeTxt = new javax.swing.JTextField();
        diretorTxt = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        anoTxt = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        disponivelCheckBox = new javax.swing.JCheckBox();
        cadastrarBtn = new javax.swing.JButton();
        generoCombo = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 51, 102));

        filmesTable.setBackground(new java.awt.Color(0, 51, 102));
        filmesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(filmesTable);

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 255, 255));
        jLabel1.setText("Nome");

        nomeTxt.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        diretorTxt.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 255, 255));
        jLabel2.setText("Diretor");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 255, 255));
        jLabel3.setText("Ano Lançamento");

        anoTxt.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 255, 255));
        jLabel4.setText("Gênero");

        disponivelCheckBox.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        disponivelCheckBox.setForeground(new java.awt.Color(0, 255, 255));
        disponivelCheckBox.setSelected(true);
        disponivelCheckBox.setText("Disponível");

        cadastrarBtn.setBackground(new java.awt.Color(0, 0, 204));
        cadastrarBtn.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        cadastrarBtn.setForeground(new java.awt.Color(0, 255, 255));
        cadastrarBtn.setText("Cadastro");
        cadastrarBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cadastrarBtnActionPerformed(evt);
            }
        });

        generoCombo.setBackground(new java.awt.Color(0, 0, 204));
        generoCombo.setForeground(new java.awt.Color(0, 255, 255));
        generoCombo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Comédia", "Ação", "Animação", "Românce", "Drama", "Suspen", "Terror", "Família" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(anoTxt, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(generoCombo, javax.swing.GroupLayout.Alignment.LEADING, 0, 109, Short.MAX_VALUE))
                            .addComponent(jLabel1)
                            .addComponent(nomeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(diretorTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(disponivelCheckBox)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cadastrarBtn)
                        .addGap(63, 63, 63)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 409, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nomeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(diretorTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(anoTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(generoCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(disponivelCheckBox)
                .addGap(18, 18, 18)
                .addComponent(cadastrarBtn)
                .addGap(31, 31, 31))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cadastrarBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cadastrarBtnActionPerformed
        
        if(nomeTxt.getText().isEmpty() || diretorTxt.getText().isEmpty() || anoTxt.getText().isEmpty()){
            
            JOptionPane.showMessageDialog(this, "É necessário preencher todos os campos", "Preenchimento dos campos", JOptionPane.WARNING_MESSAGE);
            
        }else{
            
            //campos devidamente preenchidos
            String nomeFilm = nomeTxt.getText();
            String nomeDiretor = diretorTxt.getText();
            
            //tentativa
            try{
            
                //convertendo um valor String em inteiro
                int anoLancamento = Integer.parseInt(anoTxt.getText());
                 //criando um novo filme
                Filme novoFilme = new Filme(nomeFilm, nomeDiretor, anoLancamento, (String)generoCombo.getSelectedItem(), disponivelCheckBox.isSelected());
                
                boolean sucesso = controller.cadastraFilme(novoFilme);
                
                //atualizo a interface se o cadastro na base teve sucesso
                if(sucesso){
                    
                    minhasRegras.addNovoFilme(novoFilme);
                    
                    //força a tabela da interface grafica ser atualizada
                    filmesTable.updateUI();
                    
                }else{
                    
                    JOptionPane.showMessageDialog(this, "Erro no cadastro!!!");
                    
                }
            
            }catch(NumberFormatException e){
                
                //tratar erro
                JOptionPane.showMessageDialog(this, "O ano de lancamento DEVE ser um valor NUMÉRICO do tipo INTEIRO!!", "NumberFormatException", JOptionPane.WARNING_MESSAGE);
                anoTxt.setText("");
                
            }
                
        }
        
    }//GEN-LAST:event_cadastrarBtnActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FramePrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField anoTxt;
    private javax.swing.JButton cadastrarBtn;
    private javax.swing.JTextField diretorTxt;
    private javax.swing.JCheckBox disponivelCheckBox;
    private javax.swing.JTable filmesTable;
    private javax.swing.JComboBox<String> generoCombo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField nomeTxt;
    // End of variables declaration//GEN-END:variables
}
