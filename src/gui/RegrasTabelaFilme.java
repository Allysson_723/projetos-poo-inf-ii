package gui;

import controller.FilmeController;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;
import model.Filme;

public class RegrasTabelaFilme extends AbstractTableModel{

    private Vector<Filme> filmes;
    private String cabecalho[] = {"Nome","Diret.","Ano L.", "Genero", "Disp."};
    private FilmeController controllerFilme;
    
    public RegrasTabelaFilme(){
        
        //this.filmes = new Vector<>();
        this.controllerFilme = new FilmeController();
        
        carregaFilmes();
    }
    
    private void carregaFilmes(){
        
        //futuramente invocar o controler para buscar filmes no banco...
        //this.filmes.add(new Filme("Senhor dos Aneis", "Saulo", 2005, "Ficcao", true));
        //this.filmes.add(new Filme("Super Pets", "João", 2022, "Animação", false));
        
        this.filmes = this.controllerFilme.buscaTodosFilmes();
        
    }
    
    //quantidade de linhas desenhadas na tabela
    @Override
    public int getRowCount() {
        
        return this.filmes.size();
        
    }

    //quantidade de colunas desenhadas na tabela
    @Override
    public int getColumnCount() {
        
        return 5;
        
    }

    //qual o valor de CADA célula da tabela
    @Override
    public Object getValueAt(int indiceLinha, int indiceColuna) {
        
        Filme filmeTemp = this.filmes.get(indiceLinha);
        
        switch(indiceColuna){
            case 0: return filmeTemp.getTitulo();
                //nome
            case 1: return filmeTemp.getDiretor();
                //diretor
            case 2: return filmeTemp.getAnoLancamento();
                //ano
            case 3: return filmeTemp.getGenero();
                //genero
            case 4: return filmeTemp.isDisponivel();
                //disponivel
            default: return null;
        }
    }

    //nome de cada coluna da tabela (Cabeçalho)
    @Override
    public String getColumnName(int indiceColuna) {
        return cabecalho[indiceColuna];
    }

    //Definindo quais células podem ser modificadas (editadas)
    @Override
    public boolean isCellEditable(int indiceLinha, int indiceColuna) {
        
        //todas as celulas PODEM ser modificadas
        return true;
    }

    //modificar os dados do modelo da tabela
    @Override
    public void setValueAt(Object novoValor, int indiceLinha, int indiceColuna) {
        
        Filme filmeModificar = this.filmes.get(indiceLinha);
        
        switch(indiceColuna){
            
            case 0: //modificar o NOME (setNome)
                
                filmeModificar.setTitulo((String)novoValor); break;
                
            case 1: //modificar o diretor(setDiretor)
                
                filmeModificar.setDiretor((String)novoValor); break;
                
            case 2: //modificar o anoLancamento(setAno)
                
                filmeModificar.setAnoLancamento((int)novoValor);break;
                
            case 3: //modificar o genero (setGenero)
                
                filmeModificar.setGenero((String)novoValor);break;
                
            case 4: //modificar a disponibilidade (setDisponivel)
                
                filmeModificar.setDisponivel((boolean)novoValor);
                
        }
        
        //invoco a atualização do objeto na base de dados
        controllerFilme.modificaFilme(filmeModificar);
        
    }

    @Override
    public Class<?> getColumnClass(int indiceColuna) {
        
        switch(indiceColuna){
            
            case 2: return Integer.class;
            case 4: return Boolean.class;
            default: return String.class;
            
        }
        
    }
    
    public void addNovoFilme(Filme novoFlme){
        
        this.filmes.add(novoFlme);
        
    }
    
    
    
}
