package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.Vector;
import model.Filme;
import tool.FabricaBanco;
import java.sql.Statement;

public class FilmeRepository {

    public boolean insereNovoFilme(Filme novoFilme) {
        
        String sql = "INSERT INTO filme (nome, diretor, ano, genero, disponivel) VALUES (?, ?, ?, ?, ?)";
        
        Connection conexaoBD = FabricaBanco.getConexaoPostgres();
        
        try {
            
            //configurando a transacao (?????)
            PreparedStatement transacao = conexaoBD.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            transacao.setString(1, novoFilme.getTitulo());
            transacao.setString(2, novoFilme.getDiretor());
            transacao.setInt(3, novoFilme.getAnoLancamento());
            transacao.setString(4, novoFilme.getGenero());
            transacao.setBoolean(5, novoFilme.isDisponivel());
            
            //enviamos a transacao(sql) para a base de dados
            transacao.execute();
            
            //vamos recuperar as chaves primarias geradas pelo banco
            
            ResultSet tuplaChave = transacao.getGeneratedKeys();
            //sera que houve insert (existe chave)?
            if(tuplaChave.next()){
                //setando o id gerado pelo banco no objeto cadastrado
                novoFilme.setId(tuplaChave.getInt("id"));
                return true;
                
            }
            
            return false;
            
        } catch (SQLException err) {
            
            err.printStackTrace();
            return false;
            
        }
        
    }
    

    public Vector<Filme> selectAllFilmes(){
        
        //vetor dinnamico a ser preenchido com os filmes do banco
        Vector<Filme> filmesBanco = new Vector<>();
        
        String sql = "SELECT * FROM filme";
        Connection conexao = FabricaBanco.getConexaoPostgres();
        
        try{
            
            //aqui nós criamos a transação
            PreparedStatement trans = conexao.prepareStatement(sql);
            
            //enviamos a transação para o banco e recebemos os resultados
            ResultSet tuplas = trans.executeQuery();
            
            //passar por todas as tuplas e instanciar os objeos em memória
            while(tuplas.next()){
                
                //int id, String titulo, String diretor, int anoLancamento, String genero, boolean disponivel
                
                Filme filmeBD = new Filme(tuplas.getInt("id"), tuplas.getString("nome"), tuplas.getString("diretor"), 
                        tuplas.getInt("ano"), tuplas.getString("genero"), tuplas.getBoolean("disponivel"));
                
                //add o objeto FILME para o vetor que sera retornado
                filmesBanco.add(filmeBD);
                
            }
            
        }catch(SQLException error){
            
            error.printStackTrace();
            
        }
        
        return filmesBanco;
        
    }
    
    public void updateFilme(Filme filme){
        
        String sql = "UPDATE filme SET nome=?, diretor=?, ano=?, genero=?, disponivel=?  WHERE id =?";
        
        Connection conexao = FabricaBanco.getConexaoPostgres();
        
        try{
            
            PreparedStatement trans = conexao.prepareStatement(sql);
            
            trans.setString(1, filme.getTitulo());
            trans.setString(2, filme.getDiretor());
            trans.setInt(3, filme.getAnoLancamento());
            trans.setString(4, filme.getGenero());
            trans.setBoolean(5, filme.isDisponivel());
            trans.setInt(6, filme.getId());
            
            int tuplasModificadas = trans.executeUpdate();
            
            //retornar sucesso verificando a variavel tuplasModificadas
            
        }catch(SQLException error){
            
            error.printStackTrace();
            
        }
        
    }
    
}
