package controller;

import java.util.Vector;
import model.Filme;
import repository.FilmeRepository;

public class FilmeController {
    
    private FilmeRepository repository;

    public FilmeController() {
        
        repository = new FilmeRepository();
    }
    
    public boolean cadastraFilme(Filme novoFilme){
        
        //verificando os dados do novo filme...
        if(novoFilme == null){
            
            return false;
            
        }
        if(novoFilme.getTitulo().isEmpty() || novoFilme.getDiretor().isEmpty()){
            
            return false;
            
        }
        
        //aqui vamos a nossa base de dados...
        return repository.insereNovoFilme(novoFilme);
        
    }
    
    public Vector<Filme> buscaTodosFilmes(){
        
        return repository.selectAllFilmes();
        
    }
    
    public void modificaFilme(Filme filme){
        
        //sera que o filme foi instanciado e sera que ele ESTÁ no BD
        if(filme != null && filme.getId() != 0){
            
            repository.updateFilme(filme);
            
        }
        
    }
    
}
