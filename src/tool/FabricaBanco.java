package tool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class FabricaBanco {
    
    private static Connection con;
    
    public static Connection getConexaoPostgres(){
        
        if(con == null){
        
            try {

                //ip e porta no lugar do localhost
                con = (Connection) DriverManager.getConnection("jdbc:postgresql://localhost:5432/LocadoraBD", "postgres", "postgres");

            } catch (SQLException err) {

                err.printStackTrace();
                JOptionPane.showMessageDialog(null, "Erro no momento da conexão com o banco", "Status da conexão", JOptionPane.WARNING_MESSAGE);

            }
        
        }
        
        return con;
        
    }
    
}
