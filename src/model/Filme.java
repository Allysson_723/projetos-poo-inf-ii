package model;

public class Filme {
    private int id; //utilizado para operações em banco de dados...
    private String titulo;
    private String diretor;
    private int anoLancamento;
    private String genero; //candidato a modificação: ENUMERATE
    private boolean disponivel;

    public Filme(String titulo, String diretor, int anoLancamento, String genero, boolean disponivel) {
        this.titulo = titulo;
        this.diretor = diretor;
        this.anoLancamento = anoLancamento;
        this.genero = genero;
        this.disponivel = disponivel;
    }

    //construtor para recuperar a tupla (obj) da base dados
    public Filme(int id, String titulo, String diretor, int anoLancamento, String genero, boolean disponivel) {
        this.id = id;
        this.titulo = titulo;
        this.diretor = diretor;
        this.anoLancamento = anoLancamento;
        this.genero = genero;
        this.disponivel = disponivel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDiretor() {
        return diretor;
    }

    public void setDiretor(String diretor) {
        this.diretor = diretor;
    }

    public int getAnoLancamento() {
        return anoLancamento;
    }

    public void setAnoLancamento(int anoLancamento) {
        this.anoLancamento = anoLancamento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public boolean isDisponivel() {
        return disponivel;
    }

    public void setDisponivel(boolean disponivel) {
        this.disponivel = disponivel;
    }
    
    
    
    
}
