package aula_01_sockets_tcp;

import java.net.Socket;

import javax.swing.JOptionPane;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream; 

public class Cliente {
	
    private Socket soquete;
    private ObjectOutputStream saida;
    private ObjectInputStream entrada;
	
    public Cliente(String endereco, int porta) throws Exception {
		
        super();
        //a conexão é feita automaticamente pela função soquete
        this.soquete = new Socket(endereco, porta);
        this.saida = new ObjectOutputStream(this.soquete.getOutputStream()); 
        this.entrada = new ObjectInputStream(this.soquete.getInputStream());
		
    }

    public void enviar_mensagem(Object mensagem) throws Exception {
		
        this.saida.writeObject(mensagem);
        this.saida.flush();
		
    }
	
    public Object receber_mensagem() throws Exception {
		
	return this.entrada.readObject();
		
    }
	
    public void finalizar() throws IOException {
		
	this.soquete.close();
		
    }
	
    public static void main(String[] args) throws Exception {
		
	Cliente cliente = new Cliente("10.90.37.83", 15503);
		
        String mensagem;
	String resposta;
		
        while(true){
		
            mensagem = JOptionPane.showInputDialog(null, "Qual mensagem deseja enviar ao servidor? ", "Entrada de Dados", JOptionPane.QUESTION_MESSAGE);
        
        if(mensagem.equalsIgnoreCase("sair")){
				
            cliente.enviar_mensagem(mensagem);
            break;
				
	}
	cliente.enviar_mensagem(mensagem);
	resposta = (String)cliente.receber_mensagem();
	JOptionPane.showMessageDialog(null, "Mensagem do Servidor: " + resposta, "Resposta do Servidor", JOptionPane.PLAIN_MESSAGE);
			
	if(resposta.equalsIgnoreCase("sair")) {
				
            break;
				
	}
		
    }
		
	JOptionPane.showMessageDialog(null, "Conexão finalizada!", "Status da Conexão", JOptionPane.PLAIN_MESSAGE);
	cliente.finalizar();
		
    }
	
}

