package aula_01_sockets_tcp;

import java.net.Socket;

import javax.swing.JOptionPane;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class TrataCliente {
		
    private Socket soquete_cliente;
    private ObjectOutputStream saida;
    private ObjectInputStream entrada;

    public TrataCliente(Socket soquete_cliente) throws Exception {
		
        super();
        this.soquete_cliente = soquete_cliente;
        this.saida = new ObjectOutputStream(this.soquete_cliente.getOutputStream()); 
        this.entrada = new ObjectInputStream(this.soquete_cliente.getInputStream());
		
    }
	
    public void enviar_mensagem(Object mensagem) throws Exception {
		
        this.saida.writeObject(mensagem);
        this.saida.flush();
		
    }
	
    public Object receber_mensagem() throws Exception {
		
        return this.entrada.readObject();
		
    }
	
    public void finalizar() throws IOException {
		
        this.soquete_cliente.close();
		
    }
	
    public void iniciar() throws Exception {
		
        String mensagem;
        String resposta;
		
        while(true){
        
            mensagem = (String)receber_mensagem();
            if(mensagem.equalsIgnoreCase("sair")){
				
                break;
				
            } 
            //System.out.println("Mensagem do Cliente: " + mensagem);
            JOptionPane.showMessageDialog(null, mensagem, "Mensagem do Cliente", JOptionPane.PLAIN_MESSAGE);
            resposta = JOptionPane.showInputDialog(null, "Como deseja responder o cliente? ", "Entrada de Dados", JOptionPane.QUESTION_MESSAGE);
            if(resposta.equalsIgnoreCase("sair")){
				
                enviar_mensagem(resposta);
                break;
				
            }
            enviar_mensagem(resposta);
		
        }
		
        JOptionPane.showMessageDialog(null, "Conexão finalizada!", "Status da Conexão", JOptionPane.PLAIN_MESSAGE);
        finalizar();
		
    }
	
}
