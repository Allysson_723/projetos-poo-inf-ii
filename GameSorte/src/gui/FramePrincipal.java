package gui;

import controller.JogoController;
import java.awt.Color;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import modelo.Jogador;
import modelo.Jogo;

public class FramePrincipal extends javax.swing.JFrame {
    
    private Jogo partida;
    private JogoController regraPartida;
    
    private Random gerador;

    public FramePrincipal(){
                
        //não modificar (criado pelo GUI Builder)
        initComponents();
        
        this.gerador = new Random();
        
        Jogador vetJogador[] = {new Jogador("player 1", true), new Jogador("player 2", false)};
        
        this.partida = new Jogo(vetJogador);
        this.regraPartida = new JogoController(partida);
        
        atualizaInfoJogadorVez();
        
    }
    
    //simulando a mente da CPU
    private void menteCPU(){
        
        
        SwingWorker<Object, Object> trabalhadorCPU = new SwingWorker<Object, Object>() {
            
            @Override
            
            protected Object doInBackground() throws Exception {
                
                try {
            
                //simulando um tempo para pensar...
                Thread.sleep(1000 + gerador.nextInt(1000));

                } catch (InterruptedException ex) {

                    Logger.getLogger(FramePrincipal.class.getName()).log(Level.SEVERE, null, ex);

                }

                //sortear ou guardar
                int probabilidadeAcao = gerador.nextInt(100);

                if(probabilidadeAcao < 80 || partida.getJogadorVez().getPlacarParcial() == 0){

                    //sorteia
                    sorteio();

                }else{

                    //guardar
                    guardar();

                }
                
                //este retorno pode ser utilizado no métofo fone
                return null;
                
            }
            
        };
        
        trabalhadorCPU.execute();
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        quadroPrincipal = new javax.swing.JPanel();
        quadroPlacar = new javax.swing.JPanel();
        nomePlayer1Txt = new javax.swing.JLabel();
        nomePlayer2Txt = new javax.swing.JLabel();
        player1Txt = new javax.swing.JLabel();
        player2Txt = new javax.swing.JLabel();
        quadroPlacar1 = new javax.swing.JPanel();
        placarParcial = new javax.swing.JLabel();
        ultimosNumeros = new javax.swing.JLabel();
        placarParcialTxt = new javax.swing.JLabel();
        ultNumTxt = new javax.swing.JLabel();
        dadoTxt = new javax.swing.JLabel();
        botaoSortear = new javax.swing.JButton();
        botaoGuardar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        vezPlayerTxt = new javax.swing.JLabel();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        quadroPrincipal.setBackground(new java.awt.Color(0, 51, 102));

        quadroPlacar.setBackground(new java.awt.Color(0, 0, 204));
        quadroPlacar.setForeground(new java.awt.Color(0, 153, 51));

        nomePlayer1Txt.setFont(new java.awt.Font("sansserif", 0, 24)); // NOI18N
        nomePlayer1Txt.setForeground(new java.awt.Color(255, 255, 255));
        nomePlayer1Txt.setText("Player 1:");

        nomePlayer2Txt.setFont(new java.awt.Font("sansserif", 0, 24)); // NOI18N
        nomePlayer2Txt.setForeground(new java.awt.Color(255, 255, 255));
        nomePlayer2Txt.setText("Player 2:");

        player1Txt.setFont(new java.awt.Font("sansserif", 3, 24)); // NOI18N
        player1Txt.setForeground(new java.awt.Color(0, 255, 255));
        player1Txt.setText("0");

        player2Txt.setFont(new java.awt.Font("sansserif", 3, 24)); // NOI18N
        player2Txt.setForeground(new java.awt.Color(0, 255, 255));
        player2Txt.setText("0");

        javax.swing.GroupLayout quadroPlacarLayout = new javax.swing.GroupLayout(quadroPlacar);
        quadroPlacar.setLayout(quadroPlacarLayout);
        quadroPlacarLayout.setHorizontalGroup(
            quadroPlacarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(quadroPlacarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nomePlayer1Txt)
                .addGap(18, 18, 18)
                .addComponent(player1Txt)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 464, Short.MAX_VALUE)
                .addComponent(nomePlayer2Txt)
                .addGap(18, 18, 18)
                .addComponent(player2Txt)
                .addContainerGap())
        );
        quadroPlacarLayout.setVerticalGroup(
            quadroPlacarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(quadroPlacarLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(quadroPlacarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nomePlayer1Txt)
                    .addComponent(player1Txt)
                    .addComponent(nomePlayer2Txt)
                    .addComponent(player2Txt))
                .addGap(0, 21, Short.MAX_VALUE))
        );

        quadroPlacar1.setBackground(new java.awt.Color(0, 51, 153));

        placarParcial.setFont(new java.awt.Font("sansserif", 0, 24)); // NOI18N
        placarParcial.setForeground(new java.awt.Color(255, 255, 255));
        placarParcial.setText("Placar parcial");

        ultimosNumeros.setFont(new java.awt.Font("sansserif", 0, 24)); // NOI18N
        ultimosNumeros.setForeground(new java.awt.Color(255, 255, 255));
        ultimosNumeros.setText("Últimos números:");

        placarParcialTxt.setFont(new java.awt.Font("sansserif", 3, 24)); // NOI18N
        placarParcialTxt.setForeground(new java.awt.Color(0, 255, 255));
        placarParcialTxt.setText("0");

        ultNumTxt.setFont(new java.awt.Font("sansserif", 3, 24)); // NOI18N
        ultNumTxt.setForeground(new java.awt.Color(0, 255, 255));
        ultNumTxt.setText("...");

        javax.swing.GroupLayout quadroPlacar1Layout = new javax.swing.GroupLayout(quadroPlacar1);
        quadroPlacar1.setLayout(quadroPlacar1Layout);
        quadroPlacar1Layout.setHorizontalGroup(
            quadroPlacar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(quadroPlacar1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(quadroPlacar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(quadroPlacar1Layout.createSequentialGroup()
                        .addComponent(ultimosNumeros)
                        .addGap(18, 18, 18)
                        .addComponent(ultNumTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE))
                    .addGroup(quadroPlacar1Layout.createSequentialGroup()
                        .addComponent(placarParcial)
                        .addGap(18, 18, 18)
                        .addComponent(placarParcialTxt)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        quadroPlacar1Layout.setVerticalGroup(
            quadroPlacar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(quadroPlacar1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(quadroPlacar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(placarParcial)
                    .addComponent(placarParcialTxt))
                .addGap(18, 18, 18)
                .addGroup(quadroPlacar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ultimosNumeros)
                    .addComponent(ultNumTxt))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        dadoTxt.setFont(new java.awt.Font("sansserif", 3, 112)); // NOI18N
        dadoTxt.setForeground(new java.awt.Color(51, 255, 255));
        dadoTxt.setText("  ");

        botaoSortear.setBackground(new java.awt.Color(0, 0, 204));
        botaoSortear.setFont(new java.awt.Font("sansserif", 0, 32)); // NOI18N
        botaoSortear.setForeground(new java.awt.Color(0, 255, 255));
        botaoSortear.setText("Sortear");
        botaoSortear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoSortearActionPerformed(evt);
            }
        });

        botaoGuardar.setBackground(new java.awt.Color(0, 0, 255));
        botaoGuardar.setFont(new java.awt.Font("sansserif", 0, 32)); // NOI18N
        botaoGuardar.setForeground(new java.awt.Color(0, 255, 255));
        botaoGuardar.setText("Guardar");
        botaoGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoGuardarActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 51, 153));

        jLabel1.setFont(new java.awt.Font("sansserif", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Sua vez:");

        vezPlayerTxt.setFont(new java.awt.Font("sansserif", 0, 24)); // NOI18N
        vezPlayerTxt.setForeground(new java.awt.Color(0, 255, 255));
        vezPlayerTxt.setText("Player...");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(vezPlayerTxt)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(vezPlayerTxt))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout quadroPrincipalLayout = new javax.swing.GroupLayout(quadroPrincipal);
        quadroPrincipal.setLayout(quadroPrincipalLayout);
        quadroPrincipalLayout.setHorizontalGroup(
            quadroPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(quadroPrincipalLayout.createSequentialGroup()
                .addGroup(quadroPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(quadroPrincipalLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(quadroPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(quadroPlacar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(quadroPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, quadroPrincipalLayout.createSequentialGroup()
                                    .addComponent(quadroPlacar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, quadroPrincipalLayout.createSequentialGroup()
                                    .addComponent(botaoSortear)
                                    .addGap(450, 450, 450)
                                    .addComponent(botaoGuardar)))))
                    .addGroup(quadroPrincipalLayout.createSequentialGroup()
                        .addGap(331, 331, 331)
                        .addComponent(dadoTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(10, Short.MAX_VALUE))
        );
        quadroPrincipalLayout.setVerticalGroup(
            quadroPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(quadroPrincipalLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(quadroPlacar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(quadroPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(quadroPlacar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(dadoTxt)
                .addGap(18, 18, 18)
                .addGroup(quadroPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botaoGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botaoSortear, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(quadroPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(quadroPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sorteio(){
        
        int numeroDado = gerador.nextInt(6) + 1; // ou (1, 7)
        
        //altero a interface do usuario
        dadoTxt.setText(numeroDado + "");
        
        this.regraPartida.sorteio(numeroDado);
        
        //atualizando o plcar parcial
        //partida.getJogadorVez().addPlacarParcial(numeroDado); --> regra != interface gráfica
        placarParcialTxt.setText(partida.getJogadorVez().getPlacarParcial()+ "");
        
        //add os ultimos 3 numeros sorteados
        
        //partida.getJogadorVez().addNumeroSorteado(numeroDado); --> regra != interface gráfica
        ultNumTxt.setText(partida.getJogadorVez().imprimeUlt3Numeros());
        
    }
    
    private void guardar(){
        
        Jogador jogadorAtual = partida.getJogadorVez();
        
        boolean vencedor = this.regraPartida.guardarPlacar();
        
        if(vencedor = true){
            
            //alguem ganhou
            JOptionPane.showMessageDialog(this, "Jogador: " + jogadorAtual.getNome() + " foi o vencedor",  "Parabéns!!!", JOptionPane.INFORMATION_MESSAGE);
            //fechando a nossa aplicação
            System.exit(0);
            
        }
        
        if(partida.getVez() == 1){
            
            player1Txt.setText(jogadorAtual.getPlacarTotal()+ "");
            
        }else{
            
            player2Txt.setText(jogadorAtual.getPlacarTotal()+ "");
            
        }
        
        placarParcialTxt.setText("0");
        
        atualizaInfoJogadorVez();
        
    }
    
    private void botaoGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoGuardarActionPerformed
        
        guardar();
        
    }//GEN-LAST:event_botaoGuardarActionPerformed

    private void botaoSortearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoSortearActionPerformed
        
        sorteio();
        
    }//GEN-LAST:event_botaoSortearActionPerformed

    private void atualizaInfoJogadorVez(){
        
        if(partida.getJogadorVez().isHumano()){
            
            //atualizando as cores dos players
            nomePlayer1Txt.setForeground(Color.red);
            nomePlayer2Txt.setForeground(Color.black);
            
            vezPlayerTxt.setText("Humano");
            
            //habilitando os botões do jogador
            botaoSortear.setEnabled(true);
            botaoGuardar.setEnabled(true);
            
        }else{
            
            //CPU - computador
            
            //atualizando as cores dos players
            nomePlayer1Txt.setForeground(Color.black);
            nomePlayer2Txt.setForeground(Color.red);
            
            vezPlayerTxt.setText("CPU");
            
            //desabilitando os botões do jogador
            botaoSortear.setEnabled(false);
            botaoGuardar.setEnabled(false);
            
            menteCPU();
            
        }
        
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FramePrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botaoGuardar;
    private javax.swing.JButton botaoSortear;
    private javax.swing.JLabel dadoTxt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel nomePlayer1Txt;
    private javax.swing.JLabel nomePlayer2Txt;
    private javax.swing.JLabel placarParcial;
    private javax.swing.JLabel placarParcialTxt;
    private javax.swing.JLabel player1Txt;
    private javax.swing.JLabel player2Txt;
    private javax.swing.JPanel quadroPlacar;
    private javax.swing.JPanel quadroPlacar1;
    private javax.swing.JPanel quadroPrincipal;
    private javax.swing.JLabel ultNumTxt;
    private javax.swing.JLabel ultimosNumeros;
    private javax.swing.JLabel vezPlayerTxt;
    // End of variables declaration//GEN-END:variables
}
