package modelo;

import java.util.Random;

public class Jogo {
    
    //atributos
    
    private int vezJogador; //0 e 1
    private Jogador jogadores[];
    
    //construtores

    public Jogo(Jogador[] jogadores) {
        
        this.jogadores = jogadores;
        
        //sorteando o jogador que começa
        Random geradorAleat = new Random();
        this.vezJogador = geradorAleat.nextInt(2);
        
    }
    
    //simulando a troca de turnos entre jogadores
    public void trocaVez(){
        
        //operador ternário
        vezJogador = (vezJogador == 0) ? 1 : 0;
        
    }
    
    public Jogador getJogadorVez(){
    
        return this.jogadores[this.vezJogador];
    
    }
    
    public int getVez(){
        
        return this.vezJogador;
        
    }
    
    
}
