package modelo;

import java.util.Collections;
import java.util.Vector;

public class Jogador {
    
    //atributos
    
    private String nome;
    private int placarTotal;
    private int placarParcial;
    private Vector<Integer> ult3Numeros;
    private boolean humano; //true - player, false - máquina
    
    //construtores

    public Jogador(String nome, boolean humano) {
        this.nome = nome;
        this.placarParcial = 0;
        this.placarParcial = 0;
        this.ult3Numeros = new Vector<>();
        this.humano = humano;
    }
    
    //encapsulamento

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPlacarTotal() {
        return placarTotal;
    }

    public void setPlacarTotal(int placarTotal) {
        this.placarTotal = placarTotal;
    }

    public int getPlacarParcial() {
        return placarParcial;
    }

    public void setPlacarParcial(int placarParcial) {
        this.placarParcial = placarParcial;
    }
    
    //todo get para atributos do tipo boolean é chamado de is

    public boolean isHumano() {
        return this.humano;
    }
    
    //implementar metodos de verificar se o usuario possui 3 numeros repetidos e é preciso implementar uma lista
    
    //comportamentos
    
    public void addPlacarParcial(int numeroDado){
        
        this.placarParcial += numeroDado;
        
        addNumeroSorteado(numeroDado);
        
    }
    
    public void guardarParcial(){
        
        this.placarTotal += placarParcial;
        this.placarParcial = 0;
        
    }
    
    private void addNumeroSorteado(int numeroDado){
        
        this.ult3Numeros.add(numeroDado);
        
        //verificando se 
        if(this.ult3Numeros.size() > 3){
            
            //removendo o primeiro numero
            this.ult3Numeros.remove(0);
            
        }
        
    }
    
    public String imprimeUlt3Numeros(){
        
        String textResultado = "";
        
        for(int i = 0; i < this.ult3Numeros.size(); i++){
            
            textResultado += this.ult3Numeros.get(i);
            textResultado += " ";
            
        }
        
        return textResultado;
        
    }
    
    //verificando se os últimos 3 números sorteados são iguais
    public boolean verificaUltimosSorteios(){
        
        int qNumerosIguais = Collections.frequency(ult3Numeros, ult3Numeros.get(0));
        
        return qNumerosIguais == 3;
        
    }
    
}
