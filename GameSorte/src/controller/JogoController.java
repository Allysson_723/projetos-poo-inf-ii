package controller;

import javax.swing.JOptionPane;
import modelo.Jogador;
import modelo.Jogo;

public class JogoController {
    
    private Jogo partidaCorrente;

    public JogoController(Jogo partidaCorrente) {
        
        this.partidaCorrente = partidaCorrente;
        
    }
    
    //definir as regras durante o sorteio e retornar indicando se o player atual continua a rodada
    public boolean sorteio(int numero){
        
        Jogador jogadorAtivo = partidaCorrente.getJogadorVez();
        
        //atualizei o pplacar parcial e os ultimos 3 numeros
        jogadorAtivo.addPlacarParcial(numero);
        
        //verificar as regras
        
        if(jogadorAtivo.verificaUltimosSorteios()){
            
            //jogador sorteou 3 números iguais consecutivamente
            //PERDE TUDO!!!
            //jogador perde os placares e perde a vez
            jogadorAtivo.setPlacarParcial(0);
            jogadorAtivo.setPlacarTotal(0);
            partidaCorrente.trocaVez();
            return false;
            
        }
        //dado sorteado igual a 1 perde a vez
        if(numero == 1){
            
            partidaCorrente.trocaVez();
            return false;
            
        }else if (numero == 6){
            
            jogadorAtivo.setPlacarParcial(0);
            partidaCorrente.trocaVez();
            return false;
            
        }
        
        //o jogador continua na próxima rodada
        return true;
        
    }
    
    //retorna true caso o jogador atual seja o vencedor da partida
    public boolean guardarPlacar() {
        
        Jogador jogadorAtivo = partidaCorrente.getJogadorVez();
        
        if(jogadorAtivo.getPlacarParcial() > 0){
            
            jogadorAtivo.guardarParcial();
            
            if(jogadorAtivo.getPlacarTotal() >= 20){
                
                return true;//o jogador venceu
                
            }
            
            partidaCorrente.trocaVez();
            
        }else{
            
            JOptionPane.showMessageDialog(null, "Voçê não possui valor no placar parcial para guardar!!!");
            
        }
        
        return false;//o jogador não venceu
        
    }
    
}
