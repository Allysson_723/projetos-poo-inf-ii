package testegui1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class TesteGUI1 {
    
    private static int contador = 0;
    private static JButton botaoContador;

    public static void main(String[] args) {
        
        //criando o JFrame da aplicação
        
        JFrame quadroPrincipal = new JFrame("Primeira aplicação");
        
        //configurando o tamalho (pixel)
        
        quadroPrincipal.setSize(400, 400);
        
        //centralizando o quadro no meio da tela do usuário
        
        quadroPrincipal.setLocationRelativeTo(null);
        
        //setando o quadro como visível para o usuário
        
        quadroPrincipal.setVisible(true);
        
        //fazendo com que o programa pare de executar quando o quadro for fechado (X)
        
        quadroPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //borderLayout = layout defoult: o painel colocado é esticado para ocupar todo o espaço do frame
        //temos LIBERDADE de DEFINIR a POSIÇÂO e TAMANHO do painel
        
        quadroPrincipal.setLayout(null);
        
        //add Paineis ("avisos") no frame ("quadro")
        
        JPanel aviso1 = new JPanel();
        
        //definindo a cor do fundo do painel
        
        aviso1.setBackground(Color.YELLOW);
        
        aviso1.setSize(200, 200);
        
        //colocando o painel no frame
        
        quadroPrincipal.add(aviso1);
        
        JPanel painelFundo = new JPanel();
        
        painelFundo.setBackground(Color.white);
        
        painelFundo.setSize(400, 400);
        
        quadroPrincipal.add(painelFundo);
        
        //add botões e tratando eventos
        
        JButton botaoContador = new JButton();
        
        botaoContador.setSize(70, 30);
        
        botaoContador.setLocation(200, 0);
        
        painelFundo.add(botaoContador);
        
        /*botaoContador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contador++;
                
                //atualizando o texto do botão
                
                botaoContador.setText(contador + "");
            }
        });*/
        
        botaoContador.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                
                if(e.getButton() == MouseEvent.BUTTON1){
                    
                    contador++;
                    
                    
                }else{
                    
                    contador--;
                    
                }
                
                //atualizando o texto do botão
                botaoContador.setText(contador + "");

                
            }

            @Override
            public void mousePressed(MouseEvent e) {
                
                System.out.println("Botão pressionado");
                
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                
                System.out.println("usuário soltou o botão do mouse");
                
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                
                System.out.println("setinha entrou no botão");
                
            }

            @Override
            public void mouseExited(MouseEvent e) {
                
                System.out.println("setinha saiu do botão");
                
            }
        });
        
        
        
        
        
        
        
        
        //exemplo de uma entrada de dados via JOPtionPane
        
        /*//metodos JOptionPanel são estáticos, ou seja são métodos da classe, não precisam de objetos para serem executados
        
        //messageDialog = janla de mensagem pontual
        
        //parametro 1: parent component, é um frame principal a partir do qual a janela será executaso
        //pode ser usado para bloquear a aplicação enquanto o usuáario não interagir com a janela
        //parametro 2: mensagem mostrada
        //parametro 3: titulo da janela
        //parametro 4: icone da mensagem da janela, define como a janela será apresentada
        
        //JOptionPane.showMessageDialog(null, "Opção inválida", "FATAL ERROR", JOptionPane.ERROR_MESSAGE);
        
        //confirma dialog = janela de confirmação (ok ou cancelar...)
        
        /*int opUser = JOptionPane.showConfirmDialog(null, "deseja mesmo excluir a sua conta?", "Exclusão de Conta", JOptionPane.YES_NO_CANCEL_OPTION);
        
        //verificando qual foi a opção do usuário (de forma padrão, sem usar -1, 0 e 1)
        
        if(opUser == JOptionPane.OK_OPTION){
            
            //escolha do YES
            
            JOptionPane.showInternalMessageDialog(null, "Sua conta foi excluida com sucesso!");
            
        }else if(opUser == JOptionPane.NO_OPTION){
            
            //escolha do NO
            
            JOptionPane.showInternalMessageDialog(null, "Exclusão da conta cancelada!");
            
        }else{
            
            //escolha do CANCEL
        
            JOptionPane.showInternalMessageDialog(null, "Operação cancelada!");
        
        }*/
        
        //input dialog = janela de entrada puntual de dados
        
        /*String valor = JOptionPane.showInputDialog(null, "Informe a sua idade");
        
        //a classe Integer é uma classe que possui métodos estáticos para tratamento de valores inteiros
        
        int idade = Integer.parseInt(valor);
        
        if(idade >= 16){
            
            JOptionPane.showInternalMessageDialog(null, "Você pode votar :P", "Idade válida", JOptionPane.INFORMATION_MESSAGE);
            
        }else{
            
            JOptionPane.showInternalMessageDialog(null, "Você não pode votar :D", "Idade inválida", JOptionPane.ERROR_MESSAGE);
            
        }*/
        
        //JFileChooser = janela de escolha de arquivos
        
    }
    
}
