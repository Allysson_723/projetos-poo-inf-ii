package testeenumerate;

import java.util.Scanner;

public class Principal {
    
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("Escolha a classe do heroi: ");
        
        
        for(ClasseHeroi c : ClasseHeroi.values()){
            
            System.out.println(c.getNome());
            
        }
        
        int indice = entrada.nextInt();
        ClasseHeroi classeEscolhida = ClasseHeroi.values()[indice - 1];
        
        System.out.println("A classe escolhida foi: " + classeEscolhida);
        
    }
    
}
