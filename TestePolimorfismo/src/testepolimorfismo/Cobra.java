package testepolimorfismo;

public class Cobra extends Animal{
    
    public boolean venenosa;

    public Cobra(boolean venenosa, String nome, double peso, double velocidade) {
        super(nome, peso, velocidade);
        this.venenosa = venenosa;
    }

    public boolean isVenenosa() {
        return venenosa;
    }

    public void setVenenosa(boolean venenosa) {
        this.venenosa = venenosa;
    }

    @Override
    //polimorfismo dinâmico, sobrescrevendo um método da superclasse
    //no polimorfismo dinamico o cabeçalho do metodo deve ser igual, com os mesmos parametros
    public void movimentar(double distancia) {
        
        double tempo = distancia / super.velocidade;
        System.out.println("A cobra " + super.nome + " demora: " + tempo + "seg");
        
        for(int i = 0; i < tempo; i++){
            
            System.out.print("~");
            
        }
        System.out.println("");
        
    }
    
    
    
}
