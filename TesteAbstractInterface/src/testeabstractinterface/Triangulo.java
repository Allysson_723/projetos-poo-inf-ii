package testeabstractinterface;

public class Triangulo extends FormaGeometrica implements ComponenteVisivel{
    
    private int tamBase;

    public Triangulo(int tamLado, int numLados, String simbolo) {
        
        super(numLados, simbolo);
        this.tamBase = tamLado;
        
    }

    @Override
    public void mostrar() {
    
        /*
        
           *
          ***
         *****
        
        */
        
        if(this.numLados % 2 == 0){
            
            System.out.println("Não é possível mostrar o triangulo");
            
        }else{
            
            int qLinhas = (this.tamBase / 2) + 1;
            int qEsp = this.tamBase / 2;
            
            for(int lin = 0; lin <=qLinhas; lin++){
                
                //add espacos
                
                for(int esp = 1; esp <= 10; esp++){
                
                    System.out.print("  ");
                    
                }
            
                for(int simb = 1; simb <= (lin * 2); simb++){

                    System.out.print(super.simbolo + " ");

                }

                System.out.println("");
                qEsp--;
                
            }
            
        }
    
    }
    
    
    
}
