package testeabstractinterface;

//quadrado herda de forma geometrica e implementa os metodos de componente visivel

public class Quadrado extends FormaGeometrica implements ComponenteVisivel{
    
    private int tamaLado;

    public Quadrado(int numLados, String simbolo, int tamLado) {
        
        super(numLados, simbolo);
        this.tamaLado = tamLado;
        
    }

    @Override
    public void mostrar() {
    
        for(int linha = 1; linha <= this.tamaLado; linha++){
            
            for(int simb = 1; simb <= this.tamaLado; simb++){
                
                System.out.print(super.simbolo + " ");
                
            }
            
            System.out.println("");
            
        }
        
    }

    
    
}
