package testeabstractinterface;

//interfaces não possuem atributos, construtores e encapsulamento, apenas métodos abstratos

public interface ComponenteVisivel {
    
    public abstract void mostrar();
    
}
