package testeabstractinterface;

public abstract class FormaGeometrica {
    
    protected int numLados;
    protected String simbolo;

    public FormaGeometrica(int numLados, String simbolo) {
        this.numLados = numLados;
        this.simbolo = simbolo;
    }

    public int getNumLados() {
        return numLados;
    }

    public void setNumLados(int numLados) {
        this.numLados = numLados;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }
    
    
    
}
